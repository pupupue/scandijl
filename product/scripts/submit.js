$(document).ready(function() {
    $("form").on('submit', function(e) {
        e.preventDefault();
        var sku = $("input[name='sku']").val();
        var item_name = $("input[name='item_name']").val();
        var item_price = $("input[name='item_price']").val();
        var item_type = $("#select").val();

        if(item_type == ''){
            //animate select for feedback
            $('#select').css("box-shadow", "0 0 4px #811");
            $('#select').change(function(){
                $('#select').css("box-shadow", "0 0 0px #fff");
            });
        } else if (item_type == 'Book'){
            var pages = $("input[name='pages']").val();
            var author = $("input[name='author']").val();
            var language = $("input[name='language']").val();
            // 
            $.post("../php/post.php", {// later post.php
                sku: sku,
                item_name: item_name,
                item_price: item_price,
                item_type: item_type,
                pages: pages,
                author: author,
                language: language
            }, function(data) {

                // get returnMessage id and replace content with gotten content
                $("#returnMessage").html(data);
            });
        } else if (item_type == 'Clock'){
            var height = $("input[name='height']").val();
            var width = $("input[name='width']").val();
            var length = $("input[name='length']").val();
            var weight = $("input[name='weight']").val();
            var color = $("input[name='color']").val();
            var batteries = $("input[name='batteries']").val();
            var type = $("input[name='type']").val();

            // 
            $.post("../php/post.php", {// later post.php
                sku: sku,
                item_name: item_name,
                item_price: item_price,
                item_type: item_type,
                height: height,
                width: width,
                length: length,
                weight: weight,
                color: color,
                batteries: batteries,
                type: type
            }, function(data) {
                // get returnMessage id and replace content with gotten content
                $("#returnMessage").html(data);
            });
        
        } else if (item_type == 'Furniture'){
            var height = $("input[name='height']").val();
            var width = $("input[name='width']").val();
            var length = $("input[name='length']").val();
            var weight = $("input[name='weight']").val();
            var color = $("input[name='color']").val();
            var material = $("input[name='material']").val();
            // 
            $.post("../php/post.php", {// later post.php
                sku: sku,
                item_name: item_name,
                item_price: item_price,
                item_type: item_type,
                height: height,
                width: width,
                length: length,
                weight: weight,
                color: color,
                material: material
            }, function(data) {
                // get returnMessage id and replace content with gotten content
                $("#returnMessage").html(data);
            });
        } else if (item_type == 'Lamp'){
            var height = $("input[name='height']").val();
            var width = $("input[name='width']").val();
            var length = $("input[name='length']").val();
            var weight = $("input[name='weight']").val();
            var color = $("input[name='color']").val();
            var material = $("input[name='material']").val();
            var volts = $("input[name='volts']").val();
            // 
            $.post("../php/post.php", {// later post.php
                sku: sku,
                item_name: item_name,
                item_price: item_price,
                item_type: item_type,
                height: height,
                width: width,
                length: length,
                weight: weight,
                color: color,
                material: material,
                volts: volts
            }, function(data) {
                // get returnMessage id and replace content with gotten content
                $("#returnMessage").html(data);
            });
        } else if (item_type == 'Movie'){
            var format = $("input[name='format']").val();
            var year = $("input[name='year']").val();
            var runtime = $("input[name='runtime']").val();
            var language = $("input[name='language']").val();
            // 
            $.post("../php/post.php", {// later post.php
                sku: sku,
                item_name: item_name,
                item_price: item_price,
                item_type: item_type,
                format: format,
                year: year,
                runtime: runtime,
                language: language
            }, function(data) {
                // get returnMessage id and replace content with gotten content
                $("#returnMessage").html(data);
            });
        }

        
        
    });
});