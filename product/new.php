<?php 
include_once "../php/init.php";
$page_title = "NEW";
$obj = new Item();
$item = new Item();
$break = 1;

?>
<?php include "../includes/header.php"; ?>
<!-- script in new-->
<script type='text/javascript' src='scripts/xmlhttp.js'></script>
<script type='text/javascript' src='scripts/submit.js'></script>

<div class="wrapper">
    <!-- container -->
    <div class="container">

        <div class="col-md-12">
            <div class="page-header">
                <h1>Product Add</h1>
            </div>
        </div>        

        <!-- displays ajax message -->
        <div id="returnMessage"></div>

        
            <form action="#" method="post">
                <div class='col-md-12'>
                    <div class="form-row">
                    <!-- static -->
                        <div class="col-md-4 mb-3">
                            <input type="text" name="sku" class="form-control" placeholder="SKU" value='<?php echo $item->sku?>'>
                        </div>  
                        <div class="col-md-4 mb-3">
                            <input type="text" name="item_name" class="form-control" placeholder="Name" value='<?php echo $item->item_name?>'>
                        </div>  
                        <div class="col-md-2 mb-3">
                            <input type="text" name="item_price" class="form-control" placeholder="Price"  value='<?php echo $item->item_price?>'>
                            <label for="item_price" class="">Use [99.99] format</label>
                        </div>
                        <div class="col-md-2 mb-3">
                            <input type="submit" name="submit"  id="submit" value="ADD" class="btn btn-primary form-inline">
                        </div>
                        <!-- select -->
                        <div class="row">
                            <div class="col-md-4">
                                <select name='item_type' id="select" class="form-control custom-select mr-sm-2" onchange="getType(this.value)" style="margin-bottom: 2em">
                                <?php 
                                    echo $obj->listTypes();
                                ?>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <!-- empty -->
                            </div>
                        </div><!-- /row -->
  
                        <div id="dynamic" class="form-group row">
                            <!-- dynamic -->
                        </div>
                    </div>
                </div>
                
            </form>

        </div>

    <div class="push"></div>
</div>

        <!-- /row -->
        

    <!-- /container -->
    <?php include "../includes/footer.php"; ?>
    
</body>
</html>