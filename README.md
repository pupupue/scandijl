# PHP OOP test Janis A Laurins
Created on Windows enviroment using WAMP
Implemented with jquery & ajax

## Requirements
* PHP 5.5 +
* MySQL 5 database

## Installation
* Make sure you have Apache, PHP, MySQL installed.
* Clone this repo to your server.
* To setup application import the .sql file in database
* To run application go to ../db_oop_php/
* Edit the config.php file located in root

## Task description:

The expected outcome is  2 separate pages for:
- product list
- product add

Product list should list all existing product and details, like:
- SKU ​(unique for each product)
- Name
- Price

Also each product type has special attribute, which we expect you would be able to display as well (one of based on type):
- Size (in MB) for ​DVD-disc
- Weight (in Kg) for ​Book
- Dimensions (HxWxL) for ​Furniture

An advantage​ would be implementation of the optional feature:
- mass ​delete ​action,implemented as  checkboxes next to each product.
