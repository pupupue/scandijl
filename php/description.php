<?php 

class Description 
{
    public static $attributes =
    [
        'height' => 'Please provide dimension for Height,(HxWxL)cm ',
        'width'=> 'Please provide dimension for Width (HxWxL)cm',
        'length'=> 'Please provide dimension for Length (HxWxL)cm ',
        'weight'=> 'Please provide weight in (kg)',
        'material'=> 'Please provide the Material (wood, metal, plastic, ect.)',
        'volts'=> 'Please provide the Voltage (110, 120, ect)',
        'color'=> 'Enter the products color',
        'batteries' => 'Describe the required batteries and (included/not included)',
        'pages' => 'Please enter the Page length (number)',
        'author' => 'Please enter the authors full name',
        'language' => 'specify the language/es',
        'format' => 'Please provide the format (Blu-ray, DVD, ect.)',
        'year' => 'Please enter the release year',
        'runtime' => 'Please enter the runtime eg. (1h 42min)',
        'type' => 'Please describe the product'
    ];
}


