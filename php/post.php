<?php 

include "init.php";

if($_SERVER['REQUEST_METHOD'] !='POST'){
//empty
//echo "empty";
} else {
//is post
    $item = new Item;
    //first check null for item_type
    if($_POST['item_type']== ''){
        // or load up same values 
        $item->getFields();
    } else {
        $class = $_POST['item_type'];
        // new DynamicItem;
        $item = new $class();   
        $item->getFields();

        $errors = $item->validateFields();
        if($item->passed()){
            $item->save();
            echo "<div id='returnMessage' class='col-md-12 alert alert-success'>Item saved Successfully</div>";
        } else {
            // has errors
            echo "<div id='returnMessage' class='col-md-12 alert alert-warning'>".$errors[0]."</div>";

        }
    }
}