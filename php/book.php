<?php


class Book extends Item
{
    protected static $db_attributes_table = "book";
    protected static $db_attributes_table_fields = array('pages', 'author', 'language');
    
    protected static $attribute_rules = array(
        'pages' => array(
            'required' => true,
            'int' => true
        ),
        'author'=> array(
            'required' => true,
            'symb' => true,
            'min' => 2,
            'max' => 255
        ),
        'language'=> array(
            'required' => true,
            'char' => true,
            'min' => 2,
            'max' => 20
        )
    );

    public $pages = '';
    public $author = '';
    public $language = '';

    public function showItemDetails()
    {
        echo "<b>Created by</b>: ".$this->author."<br>";
        echo "<b>Pages</b>: ".$this->pages."<br>";
        echo "<b>Written in</b>: ".$this->language."<br>";
    }

    public function validateFields()
    {
        $fields = array_merge(self::$input_fields, self::$db_attributes_table_fields);
        $all_rules = array_merge(self::$item_rules, self::$attribute_rules);
        global $db;
        foreach ($fields as $field) {
            $specific_rule = $this->findRules($field, $all_rules);
            $this->validate($field, $this->$field, $specific_rule);
        }  
        return $this->errors(); 
    }

    public function getFields()
    {
        $fields = array_merge(self::$input_fields, self::$db_attributes_table_fields);
        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                $this->$field = htmlspecialchars($_POST[$field]);
            }
        }   
    }

}
