<?php


class Movie extends Item
{
    protected static $db_attributes_table = "movie";
    protected static $db_attributes_table_fields = array('format', 'year', 'runtime', 'language');
    protected static $attribute_rules = array(
        'format' => array(
            'required' => true,
            'min' => 2,
            'max' => 255
        ),
        'year'=> array(
            'required' => true,
            'double' => true
        ),
        'runtime'=> array(
            'required' => true,
            /* 'runtime' => true */
        ),
        'language'=> array(
            'required' => true,
            'char' => true,
            'min' => 2,
            'max' => 255
        )
    );

    public $format = '';
    public $year = '';
    public $runtime = '';
    public $language = '';


    public function showItemDetails()
    {
        echo "<b>Format</b>: ".$this->format."<br>";
        echo "<b>Aired on</b>: ".$this->year."<br>";
        echo "<b>Runtime</b>: ".$this->runtime."<br>";
        echo "<b>Language</b>: ".$this->language."<br>";
    }

    public function validateFields()
    {
        $fields = array_merge(self::$input_fields, self::$db_attributes_table_fields);
        $all_rules = array_merge(self::$item_rules, self::$attribute_rules);
        global $db;
        foreach ($fields as $field) {
            $specific_rule = $this->findRules($field, $all_rules);
            $this->validate($field, $this->$field, $specific_rule);
        }
        return $this->errors();
    }

    public function getFields()
    {
        $fields = array_merge(self::$input_fields, self::$db_attributes_table_fields);
        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                $this->$field = htmlspecialchars($_POST[$field]);
            }
        }   
    }

}
